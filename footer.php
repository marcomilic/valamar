			
			<footer class="footer" role="contentinfo">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<h2 class="follow">Follow us</h2>
							<div class="valamar-sm">
								<ul>
									<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/icon-facebook.svg" />Facebook</a></li>
									<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/icon-twitter.svg" />Twitter</a></li>
									<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/icon-instagram.svg" />Instagram</a></li>
									<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/icon-youtube.svg" />Youtube</a></li>
								</ul>
							</div>	
						</div>
					</div>
				</div>

				<div class="instagram-widget">
					<?php echo do_shortcode('[elfsight_instagram_feed id="1"]'); ?>
				</div>

				<div class="container">
					<div class="row">
						<div class="col-12">

							<div class="newsletter-footer">
								<h2>SUBSCRIBE TO<br />VALAMAR NEWSLETTER</h2>
								<?php get_template_part('template-parts/footer-newsletter'); ?>
							</div>	
				
				
							<div class="footer-logo">
								<a href="<?php echo home_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/valamar-footer-logo.svg" alt="Valamar Riviera d.d"/></a>
							</div>
							
							<div class="footer-menu">
								<?php footer_nav(); ?>
							</div>
							
							<div class="copyright">Copyright &copy; <?php echo date('Y'); ?> Valamar Riviera d.d</div>				
							
							<div class="valamar-mainsite">
								<a href="https://www.valamar.com/" target="_blank">www.valamar.com</a>
							</div>
						
						</div>
					</div>
				</div>			
				
				<div class="valamar-brands">
					<div class="container">
						<div class="row">
							<div class="col-12">
								<div class="brand-list">
									<ul>
										<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/footer-logos/valamar-collection.svg" alt=""/></a></li>
										<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/footer-logos/valamar-collection-resorts.svg" alt=""/></a></li>
										<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/footer-logos/valamar-hotels-resorts.svg" alt=""/></a></li>
										<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/footer-logos/sunny.svg" alt=""/></a></li>
										<li><a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/footer-logos/camping-adriatic.svg" alt=""/></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>

				

			</footer>
			



		<?php wp_footer(); ?>

	</body>
</html>
