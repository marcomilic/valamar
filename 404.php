<?php get_header(); ?>

	<main role="main">

		<section>		
			<article id="post-404">

				<h1><?php _e( 'Page not found', 'mweb' ); ?></h1>
				<h2>
					<a href="<?php echo home_url(); ?>"><?php _e( 'Return home?', 'mweb' ); ?></a>
				</h2>

			</article>
			<!-- /article -->

		</section>
		<!-- /section -->
	</main>

<?php get_footer(); ?>