<?php if (isset($_GET['mweb'])): ?>


<?php get_header(); /* Template Name: Destinations */ ?>

<div class="page-hero destinations">
	<div class="hero-title">
		<h1><?php the_title(); ?></h1>
	</div>	
</div>

<div class="content-section">
	<div class="container">

		<div class="row recent">
			<div class="col-12">
				<h2 class="section-wave-title">Recent</h2>
			</div>
			<div class="col-md-6">
				<div class="news-item-regular">
					<div class="featured-image">
						<a href="#">
							<img src="<?php echo get_template_directory_uri(); ?>/images/dump-images/news-item-regular.jpg" alt="" />
						</a>
					</div>	
					<div class="news-cat">DESTINATIONS</div>
					<h3><a href="#">Discover Why Rab Is the Happy Island</a></h3>
					<div class="post-meta">
						<div class="post-date">15.08.2019</div>
						<div class="reading-time"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/icon-reading-time.svg" alt="">2 min</div>
					</div>
				</div>				
			</div>


			<div class="col-md-6">
				<div class="news-item-regular">
					<div class="featured-image">
						<a href="#">
							<img src="<?php echo get_template_directory_uri(); ?>/images/dump-images/news-item-regular.jpg" alt="" />
						</a>
					</div>
					<div class="news-cat">DESTINATIONS</div>
					<h3><a href="#">Discover Why Rab Is the Happy Island</a></h3>
					<div class="post-meta">
						<div class="post-date">15.08.2019</div>
						<div class="reading-time"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/icon-reading-time.svg" alt="">2 min</div>
					</div>
				</div>				
			</div>
		</div><!-- / recent -->


		<div class="row featured">
			<div class="col-12">
				<h2 class="section-wave-title">Featured</h2>

				<div class="featured-post">
					<img src="<?php echo get_template_directory_uri(); ?>/images/news-featured-posts.jpg" alt="" />
					<div class="content-box">
						<div class="news-cat">DESTINATIONS</div>
							<h3>A Guide to King’s Landinn Game of Thrones Filming Locations in Dubrovnik</h3>
							<div class="post-meta">
								<div class="post-date">15.08.2019</div>
								<div class="reading-time"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/icon-reading-time.svg" alt="">2 min</div>
							</div>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempor vitae libero a imperdiet. Aenean consectetur venenatis eros, vestibulum facil.</p>
							<a href="" class="btn center gold ">Read more</a>
					</div>	
				</div>
			</div>
		</div>	


		<div class="row recent">
			<div class="col-12">
				<h2 class="section-wave-title">Recent</h2>
			</div>
			<div class="col-md-6">
				<div class="news-item-regular">
					<div class="featured-image">
						<a href="#">
							<img src="<?php echo get_template_directory_uri(); ?>/images/dump-images/news-item-regular.jpg" alt="" />
						</a>
					</div>	
					<div class="news-cat">DESTINATIONS</div>
					<h3><a href="#">Discover Why Rab Is the Happy Island</a></h3>
					<div class="post-meta">
						<div class="post-date">15.08.2019</div>
						<div class="reading-time"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/icon-reading-time.svg" alt="">2 min</div>
					</div>
				</div>				
			</div>


			<div class="col-md-6">
				<div class="news-item-regular">
					<div class="featured-image">
						<a href="#">
							<img src="<?php echo get_template_directory_uri(); ?>/images/dump-images/news-item-regular.jpg" alt="" />
						</a>
					</div>
					<div class="news-cat">DESTINATIONS</div>
					<h3><a href="#">Discover Why Rab Is the Happy Island</a></h3>
					<div class="post-meta">
						<div class="post-date">15.08.2019</div>
						<div class="reading-time"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/icon-reading-time.svg" alt="">2 min</div>
					</div>
				</div>				
			</div>
		</div><!-- / recent -->



	</div><!-- /container -->

</div>

<?php get_footer(); ?>


<?php  else: ?>
<h1>Developer mode</h1> 
<?php endif; ?> 