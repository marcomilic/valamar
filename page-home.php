<?php if (isset($_GET['mweb'])): ?>


<?php get_header(); /* Template Name: Homepage */ ?>

<div id="fullpage">

	<div class="section" id="section0">
		<video id="myVideo" loop muted data-autoplay>
			<source src="<?php echo get_template_directory_uri(); ?>/video/video-naslovnica.mp4" type="video/mp4">
		</video>
	</div>

	<div class="section category-cards" id="section1">		
		<div class="card-slider owl-carousel owl-theme">
			<div class="card-item card-destination">
				<a href="<?php echo get_page_link(22); ?>">
					<div class="card-title">
						<h2>Destinations</h2>
					</div>
				</a>
			</div>
			<div class="card-item card-experiences">
				<a href="<?php echo get_page_link(24); ?>">
					<div class="card-title">
						<h2>Experiences</h2>
					</div>	
				</a>	
			</div>
			<div class="card-item card-events">
				<a href="<?php echo get_page_link(26); ?>">
					<div class="card-title">
						<h2>Events</h2>
					</div>
				</a>
			</div>
		</div>
	</div><!-- section 1 -->

	<div class="section latest-news" id="section2">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2 class="section-wave-title">Recent</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="news-item">
						<a href="#">
							<img src="<?php echo get_template_directory_uri(); ?>/images/latest-news.jpg" alt="" />
							<div class="item-content">
								<div class="content-box">
									<div class="news-cat">DESTINATIONS</div>
									<h3>Discover Why Rab Is the Happy Island</h3>
									<div class="post-meta">
										<div class="post-date">15.08.2019</div>
										<div class="reading-time"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/icon-reading-time.svg" alt="">2 min</div>
									</div>
								</div>
							</div>
						</a>
					</div>				
				</div>


				<div class="col-md-6">
					<div class="news-item">
						<a href="#">
							<img src="<?php echo get_template_directory_uri(); ?>/images/latest-news.jpg" alt="" />
							<div class="item-content">
								<div class="content-box">
									<div class="news-cat">DESTINATIONS</div>
									<h3>Discover Why Rab Is the Happy Island</h3>
									<div class="post-meta">
										<div class="post-date">15.08.2019</div>
										<div class="reading-time"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/icon-reading-time.svg" alt="">2 min</div>
									</div>
								</div>
							</div>
						</a>
					</div>				
				</div>


				<div class="col-md-6">
					<div class="news-item">
						<a href="#">
							<img src="<?php echo get_template_directory_uri(); ?>/images/latest-news.jpg" alt="" />
							<div class="item-content">
								<div class="content-box">
									<div class="news-cat">DESTINATIONS</div>
									<h3>Discover Why Rab Is the Happy Island</h3>
									<div class="post-meta">
										<div class="post-date">15.08.2019</div>
										<div class="reading-time"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/icon-reading-time.svg" alt="">2 min</div>
									</div>
								</div>
							</div>
						</a>
					</div>				
				</div>				


				<div class="col-md-6">
					<div class="news-item">
						<a href="#">
							<img src="<?php echo get_template_directory_uri(); ?>/images/latest-news.jpg" alt="" />
							<div class="item-content">
								<div class="content-box">
									<div class="news-cat">DESTINATIONS</div>
									<h3>Discover Why Rab Is the Happy Island</h3>
									<div class="post-meta">
										<div class="post-date">15.08.2019</div>
										<div class="reading-time"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/icon-reading-time.svg" alt="">2 min</div>
									</div>
								</div>
							</div>
						</a>
					</div>				
				</div>

			</div>
		</div>	
	</div><!-- section 2 -->

	<div class="section while-in-croatia" id="section3">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="wic-box">
						<div class="inner-box">
							<h2>While in Croatia</h2>
							<div class="wic-items owl-carousel owl-theme">
								<div class="wic-item">
									<h3>Take a Wine Road!</h3>
									<p>Croatia is famously a land of most excellent wines and most wineries are open for visitors to come and try their red, white or rosé best. Check out your maps, ask the locals or just follow the guideposts but be sure to hit all the picturesque wine roads on your way. Bring a designated driver along, though!</p>
									<a href="#" class="btn center gold">Read more</a>
								</div>
								<div class="wic-item">
									<h3>Take a Wine Road!</h3>
									<p>Croatia is famously a land of most excellent wines and most wineries are open for visitors to come and try their red, white or rosé best. Check out your maps, ask the locals or just follow the guideposts but be sure to hit all the picturesque wine roads on your way. Bring a designated driver along, though!</p>
									<a href="#" class="btn center gold">Read more</a>
								</div>
								<div class="wic-item">
									<h3>Take a Wine Road!</h3>
									<p>Croatia is famously a land of most excellent wines and most wineries are open for visitors to come and try their red, white or rosé best. Check out your maps, ask the locals or just follow the guideposts but be sure to hit all the picturesque wine roads on your way. Bring a designated driver along, though!</p>
									<a href="#" class="btn center gold">Read more</a>
								</div>
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</div><!-- /section3 -->

	<div class="section featured-posts" id="section4">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2 class="section-wave-title">Featured</h2>

					<div class="featured-post">
						<img src="<?php echo get_template_directory_uri(); ?>/images/news-featured-posts.jpg" alt="" />
						<div class="content-box">
							<div class="news-cat">DESTINATIONS</div>
								<h3>A Guide to King’s Landinn Game of Thrones Filming Locations in Dubrovnik</h3>
								<div class="post-meta">
									<div class="post-date">15.08.2019</div>
									<div class="reading-time"><img src="<?php echo get_template_directory_uri(); ?>/images/icons/icon-reading-time.svg" alt="">2 min</div>
								</div>
								<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempor vitae libero a imperdiet. Aenean consectetur venenatis eros, vestibulum facil.</p>
								<a href="" class="btn center gold ">Read more</a>
						</div>	
					</div>
				</div>

				<div class="col-12">
					<h2 class="section-wave-title">Related</h2>
				</div>

				<div class="col-md-4 small-post-item">
					<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/news-small-pic.jpg" alt=""/></a>
					<h3><a href="">Top 5 Instagram-ready Bee Dubrovnik</a></h3>
				</div>
				<div class="col-md-4 small-post-item">
					<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/news-small-pic.jpg" alt=""/></a>
					<h3><a href="">Top 5 Instagram-ready Bee Dubrovnik</a></h3>
				</div>	
				<div class="col-md-4 small-post-item">
					<a href="#"><img src="<?php echo get_template_directory_uri(); ?>/images/news-small-pic.jpg" alt=""/></a>
					<h3><a href="">Top 5 Instagram-ready Bee Dubrovnik</a></h3>
				</div>	
			</div>
		</div>
	</div>

	<div class="section trivia" id="section5">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="wic-box">
						<div class="inner-box">
							<h2>Trivia</h2>
							<div class="wic-items owl-carousel owl-theme">
								<div class="wic-item">
									
								</div>
								
							</div>
						</div>	
					</div>
				</div>
			</div>
		</div>
	</div><!-- /section5 -->	


	<div class="section take-a-look" id="section6">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<h2 class="section-wave-title">Take a look</h2>
					<div class="featured-post">
						<img src="<?php echo get_template_directory_uri(); ?>/images/take-a-look.jpg" alt="" />
	
					</div>
					<div class="content-box">
						<h3>A Tour of Girandella: See Beaches of Gorgeous Rabaa</h3>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque tempor vitae libero a imperdiet. Aenean consectetur venenatis eros, vestibulum facil.</p>
						<a href="#" class="btn center gold ">Read more</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="section" id="section7">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="testimonials-box">
						<h2>Testimonials</h2>
						<div class="testimonials-items owl-carousel owl-theme">
							<div class="testimonials-item">
								<p>Me and my family loved Croatia and the Adriatic coast so much! Everything from the food, people, beaches, historical monuments was exceptional. We'll definitely come back one day and we hope that will be soon because our kids miss their Croatian friends and we miss the good food and wine.</p>
								<div class="t-name">JOHN DOE</div>
							</div>
							<div class="testimonials-item">
								<p>Me and my family loved Croatia and the Adriatic coast so much! Everything from the food, people, beaches, historical monuments was exceptional. We'll definitely come back one day and we hope that will be soon because our kids miss their Croatian friends and we miss the good food and wine.</p>
								<div class="t-name">JOHN DOE</div>
							</div>	
							<div class="testimonials-item">
								<p>Me and my family loved Croatia and the Adriatic coast so much! Everything from the food, people, beaches, historical monuments was exceptional. We'll definitely come back one day and we hope that will be soon because our kids miss their Croatian friends and we miss the good food and wine.</p>
								<div class="t-name">JOHN DOE</div>
							</div>	
							<div class="testimonials-item">
								<p>Me and my family loved Croatia and the Adriatic coast so much! Everything from the food, people, beaches, historical monuments was exceptional. We'll definitely come back one day and we hope that will be soon because our kids miss their Croatian friends and we miss the good food and wine.</p>
								<div class="t-name">JOHN DOE</div>
							</div>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</div><!-- /section7 -->

	<div class="section" id="section8">

		<?php get_footer(); ?>

	</div>

</div>




<?php  else: ?>
<h1>Developer mode</h1> 
<?php endif; ?> 